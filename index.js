/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App.js';
import {name as appName} from './app.json';
import messaging from '@react-native-firebase/messaging';

// Register background handler
messaging().setBackgroundMessageHandler(async remoteMessage => {
  console.log('Message handled in the background!', remoteMessage);
});

messaging()
  .subscribeToTopic('product-info')
  .then(() => {
    // console.log('Subscribed to topic!')
  })

// messaging()
//   .getInitialNotification()
//   .then(remoteMessage => {
//     console.log('getInitialNotification message:',remoteMessage); // always prints null
//     if (remoteMessage) {
//       // Never reached
//       Alert.alert('here');
//       console.log(
//         'Notification caused app to open from quit state:',
//         remoteMessage,
//       );
//     }
//   });

// messaging().onNotificationOpenedApp(remoteMessage => {
//   console.log('notititititi', remoteMessage);
// });

AppRegistry.registerComponent(appName, () => App);
