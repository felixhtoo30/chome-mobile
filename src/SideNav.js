import React, { useEffect, useRef, useState } from 'react';
import { View, ScrollView, Text, Image, useWindowDimensions, StyleSheet } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';

import LogoSmCHome from './assets/images/chome-logo-sm.png';
import LogoMdCHome from './assets/images/chome-logo-md.png';

import { HeaderLeft, HeaderRight } from './components/Header';
import {
  Home,
  Categories,
  ContactUs,
  Rules,
  HowToUse,
  MapLocation,
  BankAcc,
  Settings,
  MainWebView
} from './containers';

const Drawer = createDrawerNavigator();

const SideNavigator = ({route, navigation}) => {
  const dimensions = useWindowDimensions();

  const isMdScreen = dimensions.width >= 768;

  const webViewScreens = ['Home', 'Categories'];

  const [injectCode, setInjectCode] = useState(``);

  // useEffect(() => {
  //   console.log(injectCode);
  // }, [injectCode])

  const drawerOptions = (props) => ({
    // headerShown: true,
    headerShown: false,
    headerTitle: () => {
      return <Image style={{ width: 92, height: 52 }} source={LogoSmCHome} />
    },
    headerTitleStyle: { paddingBottom: 40 },
    headerTitleAlign: isMdScreen ? 'left' : 'center',
    headerStyle: { height: 72 },
    headerLeft: () => (isMdScreen ? '' : <HeaderLeft {...props} />),
    headerRight: () => (webViewScreens.includes(props.route.name) ? <HeaderRight setInjectCode={setInjectCode} /> : ''),
  });

  const CustomDrawerContent = (props) => (
    <DrawerContentScrollView {...props}>
      <View style={styles.drawerHeader}>
        <Image source={LogoMdCHome} style={styles.drawerHeaderLogo} />
        <Text style={styles.drawerHeaderTitle}>
          {`C-Home Co., Ltd. (Building Material & Door Accessories Trading)`}
        </Text>
        <Text style={styles.drawerHeaderTagline}>
          သံပန်း၊ သံတံခါးသုံးပစ္စည်း၊ စတီးနှင့်အိမ်ဆောက်ပစ္စည်းရောင်း၀ယ်ရေး
        </Text>
      </View>
      <DrawerItemList {...props} onPress={(props) => console.log('from DrawerItemList', props)} />
    </DrawerContentScrollView>
  );

  return (
    <NavigationContainer>
      <Drawer.Navigator
        initialRouteName="Home"
        drawerType={isMdScreen ? 'permanent' : 'front'}
        drawerContent={(props) => <CustomDrawerContent {...props} />}
      >
        <Drawer.Screen
          name="Home"
          options={drawerOptions}
          initialParams={{ urlPath: 'home' }}
        >
          {(props) => <MainWebView setInjectCode={setInjectCode} {...props} />}
        </Drawer.Screen>
        
        <Drawer.Screen
          name="Categories"
          options={drawerOptions}
          initialParams={{ urlPath: 'categories' }}
        >
          {(props) => <MainWebView setInjectCode={setInjectCode} {...props} />}
        </Drawer.Screen>

        <Drawer.Screen
          name="Rules & Regulations"
          component={Rules}
          options={drawerOptions}
        />

        <Drawer.Screen
          name="Contact Us"
          component={ContactUs}
          options={drawerOptions}
        />

        <Drawer.Screen
          name="Map Location"
          component={MapLocation}
          options={drawerOptions}
        />

        <Drawer.Screen
          name="Bank Accounts"
          component={BankAcc}
          options={drawerOptions}
        />

        <Drawer.Screen
          name="How to Use"
          component={HowToUse}
          options={drawerOptions}
        />

      </Drawer.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  drawerHeader: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginVertical: 14,
    marginRight: 10,
    marginLeft: 8,
  },
  drawerHeaderLogo: {
    width: 96,
    height: 60,
    marginRight: 4,
  },
  drawerHeaderTitle: {
    flex: 1,
    flexWrap: 'wrap',
    textAlign: 'right',
    lineHeight: 22,
  },
  drawerHeaderTagline: {
    textAlign: 'right',
    marginTop: 12,
    lineHeight: 22,
    fontSize: 12,
  },
});

export default SideNavigator;
