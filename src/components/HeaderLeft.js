import React from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

const HeaderLeft = () => {
  return (
    <>
    <Icon
      name="menu"
      style={styles.topBarIcon}
    />
    </>
  );
};

const styles = StyleSheet.create({
  topBarIcon: {
    paddingLeft: 12,
    paddingRight: 12,
    color: '#0d8ed8',
    fontWeight: '700',
    fontSize: 22,
  },
});

export default HeaderLeft;
