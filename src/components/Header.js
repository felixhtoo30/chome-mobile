import React, { useRef } from 'react';
import { View, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';

const HeaderLeft = ({ navigation }) => {
  return (
    <Icon
      name="menu"
      type="feather"
      iconStyle={styles.topBarIcon}
      onPress={() => {
        navigation.openDrawer();
      }}
    />
  );
};

const HeaderRight = ({ setInjectCode }) => {
  const clickSidebarCart = `
    document.querySelector('.cart-dropdown a').click();
  `;
  const clickSearch = `
    document.querySelector('.search-device-mobile .show-search').click();
  `
  const handlePressCart = () => {
    setInjectCode((injectCode) => {
      // console.log('setInjectCode >>>', injectCode);
      return clickSidebarCart;
    });
  }
  const handlePressSearch = () => {
    setInjectCode((injectCode) => {
      // console.log('setInjectCode >>>', injectCode);
      return clickSearch;
    });
  }
  return (
    <View style={styles.headerRight}>
      <Icon 
        name="search"
        type="feather"
        iconStyle={styles.topBarIcon}
        onPress={handlePressSearch}
      />
      <Icon
        name="shopping-bag"
        type="feather"
        iconStyle={styles.topBarIcon}
        onPress={handlePressCart}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  topBarIcon: {
    paddingHorizontal: 12,
    height: 72,
    textAlignVertical: 'center',
    color: '#0d8ed8',
  },
  headerRight: {
    flex: 1,
    flexDirection: 'row'
  }
});

export { HeaderLeft, HeaderRight };
