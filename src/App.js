import React, {useState, useEffect} from 'react';
import { View, SafeAreaView, Text } from 'react-native';
import {Icon} from 'react-native-elements';
import SideNavigator from './SideNav';
import RNBootSplash from "react-native-bootsplash";
import NetInfo, {useNetInfo} from "@react-native-community/netinfo";

const App = () => {
  const netInfo = useNetInfo();
  
  useEffect(() => {
    setTimeout(() => RNBootSplash.hide({fade: true}), 1200);
  }, []);

  const ErrorView = () => {
    return (
      <View style = {{
        position: 'absolute',
        left: 0, 
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        zIndex: 99,
      }}>
        <Icon name='wifi-off' type='feather' color='#0d8ed8' size={62} />
        <Text style={{fontSize: 22, textAlign: 'center', marginTop: 28, lineHeight: 24, fontWeight: 'bold'}}>No Internet</Text>
        <Text style={{fontSize: 16, textAlign: 'center', lineHeight: 24, width: '82%', marginVertical: 12}}>Check your internet connection and try again</Text>
      </View>
    )
  };

  return (
    <SafeAreaView style={{ flex: 1, flexDirection: 'column' }}>
      {console.log(netInfo)}
      { netInfo.isConnected ? <SideNavigator /> : <ErrorView /> }
    </SafeAreaView>
  );
};

export default App;
