import Home from './Home';
import Categories from './Categories';
import ContactUs from './ContactUs';
import Settings from './Settings';
import Rules from './Rules';
import MapLocation from './MapLocation';
import HowToUse from './HowToUse';
import BankAcc from './BankAcc';
import MainWebView from './MainWebView';

export {Home, Categories, ContactUs, Settings, Rules, HowToUse, BankAcc, MapLocation, MainWebView};
