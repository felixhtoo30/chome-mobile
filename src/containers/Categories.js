import React from 'react';
// import {View, StyleSheet, Text, Pressable, ScrollView} from 'react-native';
import WebView from 'react-native-webview';

const Categories = () => {

  return (
    <WebView source={{uri: 'https://chome.centurylinksmm.com/categories'}} />
  );
};

export default Categories;
