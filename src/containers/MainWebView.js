import React, { useRef, useState, useCallback, useEffect } from 'react';
import { View, RefreshControl, Text, Dimensions, BackHandler, Alert, ActivityIndicator, ScrollView, TouchableHighlight, NetInfo } from 'react-native';
import {Icon} from 'react-native-elements';
import { WebView, WebViewNavigation } from 'react-native-webview';
import messaging from '@react-native-firebase/messaging';

const mainUrl = 'https://chome.centurylinksmm.com/';

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const MainWebView = ({ setInjectCode }) => {
  let webview = useRef(null);

  const [refreshing, setRefreshing] = useState(false);
  const [height, setHeight] = useState(Dimensions.get('screen').height);
  const [isEnabled, setEnabled] = useState(typeof onRefresh === 'function' && refreshing);

  const onRefresh = () => {
    setRefreshing(true);
    wait(500).then(() => setRefreshing(false));
    webview.current.reload();
  };

  const handleNavigationState = (newNavState) => {
    const { url } = newNavState;
    if (!url) return;

    if (url.loading) {
      // const newURL = 'https://reactnative.dev/';
      // const redirectTo = 'window.location = "' + newURL + '"';
      // webview.injectJavaScript(redirectTo);
    }
  };

  const backAction = () => {
    if (webview.current.startUrl == mainUrl) {
      confirmExitApp();
    } else {
      webview.current.goBack();
    }
    return true; // prevent default behavior (exiting app)
  };

  const confirmExitApp = () => {
    Alert.alert('Exit from CHome', 'Are u sure you want to exit?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {
        text: 'Yes',
        onPress: () => BackHandler.exitApp(),
      },
    ]);
    return true; // prevent default behavior (exiting app)
  };

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    const unsubscribe = messaging().onMessage(async remoteMessage => {
      // JSON.stringify(remoteMessage)
      Alert.alert(remoteMessage.notification.title, remoteMessage.notification.body);
    });

    // messaging().onNotificationOpenedApp(remoteMessage => {
    //   // The below code gets never executed
    //   Alert.alert('here');
    //   console.log(
    //     'Notification caused app to open from background state:',
    //     remoteMessage,
    //   );
    // });

    return () => {
      backHandler.remove();
      unsubscribe;
    }
  }, []);

  if (webview.current) {
    setTimeout(() => {
      setInjectCode((injectCode) => {
        webview.current.injectJavaScript(injectCode);
        return ``;
      })
    }, 0);
  }

  const LoadingView = () => (
    <View style = {{
      position: 'absolute',
      left: 0, 
      right: 0, 
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#ffffff',
      zIndex: 99,
    }}>
      <ActivityIndicator size="large" color="#0d8ed8" />
    </View>
  );

  const ErrorView = ({eInfo}) => {
    let errorMsg = '';
    if (eInfo.errorDesc == 'net::ERR_INTERNET_DISCONNECTED') {
      errorMsg = 'You\'re Offline Now.\nCheck your data or wifi connection';
    } else if (eInfo.errorDesc == 'net::ERR_TIMED_OUT') {
      errorMsg = "Connection time out";
    } else {
      errorMsg = "Unknown error occurred";
    }
    return (
      <View style = {{
        position: 'absolute',
        left: 0, 
        right: 0, 
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        zIndex: 99,
      }}>
        <Icon name='alert-circle' type='feather' color='#0d8ed8' size={62} />
        <Text style={{fontSize: 16, textAlign: 'center', marginVertical: 28, lineHeight: 24}}>{errorMsg}</Text>
        <TouchableHighlight
          style={{
            backgroundColor: '#0d8ed8',
            paddingVertical: 10,
            paddingHorizontal: 16
          }}
          activeOpacity={1}
          underlayColor='#047ec4'
          accessibilityLabel="No"
          onPress={() => webview.current.reload()}
        >
          <Text style={{color: '#fff'}}>Try Again</Text>
        </TouchableHighlight>
      </View>
    )
  };

  // const displayError = () => {
  //   Alert.alert(
  //     "no_internet",
  //     "require_internet_connection",
  //     [
  //       { text: 'OK', onPress: () => this.props.navigation.goBack() },
  //     ],
  //     { cancelable: false });
  // }

  return (
    <View style={{flex: 1}}>
      <ScrollView 
        style={{
          backgroundColor : 'white',
          position: 'relative'
        }}
        contentContainerStyle={{flex: 1}}
        onLayout={(e) => setHeight(e.nativeEvent.layout.height)}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            enabled={isEnabled}
            colors={["#0d8ed8"]}
            progressViewOffset={108}
          />
        }
      >
        <WebView 
          onError={(e) => (<ErrorView eInfo={e} />)}
          style={{position: 'relative'}}
          ref={(ref) => webview.current = ref}
          source={{ uri: mainUrl }}
          startInLoadingState={true}
          renderLoading={() => <LoadingView />}
          renderError={(errorDomain, errorCode, errorDesc) => <ErrorView eInfo={{errorDomain, errorCode, errorDesc}} />}
          onMessage={() => { }}
          onScroll={(e) =>
            setEnabled(
              typeof onRefresh === 'function' &&
                e.nativeEvent.contentOffset.y === 0
            )
          }
        />
      </ScrollView>
    </View>
  );
};

export default MainWebView;
