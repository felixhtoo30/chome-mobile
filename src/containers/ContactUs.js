import React from 'react';
import {View, ScrollView, Text, StyleSheet} from 'react-native';
import {Icon} from 'react-native-elements';
import tw from 'tailwind-react-native-classnames';

const ContactUs = () => {

  const contactInfoList = [
    {
      'label': 'Address',
      'content': 'No.48, Room 6-7, Nilar 7th, Saw Bwar Gyi Kone, Yangon',
      'icon': 'map-pin'
    },
    {
      'label': 'Phone',
      'content': '09-788-881200',
      'icon': 'phone'
    },
    {
      'label': 'Opening Time',
      'content': 'Monday ~ Friday (9 a.m - 5:00 p.m)',
      'icon': 'clock'
    },
  ]

  const CardItemElement = (contactInfo) => {
    return (
      <View style={styles.cardItemWrapper}>
        <Icon name={contactInfo.icon} type="feather" iconStyle={styles.icon} />
        <View style={styles.textWrapper}>
          <Text style={styles.subTitle}>{contactInfo.label}</Text>
          <Text style={styles.content} selectable>
            {contactInfo.content}
          </Text>
        </View>
      </View>
    );
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.mainTitle}>Contact Us</Text>
      <View style={styles.card, tw`shadow-lg`} selectable>
        {contactInfoList.map((contactInfo, index) => {
          return (<CardItemElement {...contactInfo} key={index} />)
        })}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  card: {
    borderRadius: 6,
    backgroundColor: '#fff',
    margin: 18,
    padding: 18,
    paddingBottom: 46,
  },
  cardItemWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginVertical: 12,
  },
  icon: {
    marginTop: 2,
    fontSize: 21
  },
  textWrapper: {
    marginHorizontal: 16,
  },
  mainTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    marginVertical: 24,
    textAlign: 'center',
  },
  subTitle: {fontSize: 16, fontWeight: 'bold', marginBottom: 4},
  content: {fontSize: 15}
});

export default ContactUs;
