import React from 'react';
import {useWindowDimensions, StyleSheet} from 'react-native';
import WebView from 'react-native-webview';
import MapView, {Marker} from 'react-native-maps';

const MapLocation = () => {
  return (
    <>
    <WebView
      source={{
        // html: `<iframe
        // src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d477.20695000568577!2d96.12338243006626!3d16.892922933983897!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30c195283bcf7405%3A0x3c4005048783b9f8!2sCHO%20SBG%20(Building%20Materials%20%26%20Door%20Accessories%20Trading)!5e0!3m2!1sen!2ssg!4v1625122124118!5m2!1sen!2ssg"
        // style="border:0; width: 100%; height: 100%; transform: scale(1);"
        // loading="lazy"></iframe>`,
        html: `<iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d954.4145330915628!2d96.12315511330964!3d16.89279778766454!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30c195283bcf7405%3A0x3c4005048783b9f8!2sCHO%20SBG%20(Building%20Materials%20%26%20Door%20Accessories%20Trading)!5e0!3m2!1sen!2ssg!4v1625145218424!5m2!1sen!2ssg"
        style="border:0; width: 100%; height: 100%; transform: scale(1);"
        loading="lazy"></iframe>`,
      }}
      scalesPageToFit={false}
    />
    </>
    // <>
    //   <MapView
    //     region={{
    //       latitude: 16.89288278575175,
    //       longitude: 96.12375122257023,
    //       latitudeDelta: 0.002,
    //       longitudeDelta: 0.001,
    //     }}
    //     style={styles.map}
    //     userLocationAnnotationTitle="what"
    //   />
    //   <Marker
    //     coordinate={{latitude: 16.89288278575175, longitude: 96.12375122257023}}
    //     pinColor='#ff0000'
    //   />
    // </>
  );
};

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default MapLocation;
