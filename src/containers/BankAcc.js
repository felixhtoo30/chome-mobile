import React from 'react';
import {View, ScrollView, Text, StyleSheet} from 'react-native';
import {Icon} from 'react-native-elements';
import tw from 'tailwind-react-native-classnames';

const BankAcc = () => {
  const bankAccList = [
    {
      name: 'ရိုးမဘဏ်',
      accNum: '0034-451-90500093',
    },
    {
      name: 'ကမ္ဘာ့ရတနာဘဏ်(၂)',
      accNum: '1121710000294',
    },
    {
      name: 'ကမ္ဘောဇဘဏ်(၂)',
      accNum: '157-301-15700107001',
    },
    {
      name: 'ဧရာ၀တီဘဏ်',
      accNum: '55201010045982',
    },
    {
      name: 'မြန်မာ့ရှေ့ဆောင်ဘဏ်',
      accNum: '0830131083000163024',
    },
    {
      name: 'CBဘဏ်',
      accNum: '2006600500036463',
    },
    {
      name: 'AGDဘဏ်',
      accNum: '3590001000259019',
    },
    {
      name: 'မြ၀တီဘဏ်',
      accNum: 'S100K0201000658U',
    },
  ];
  const CardItemElement = (bankAcc) => {
    return (
      <View style={styles.cardItemWrapper}>
        <Icon name="credit-card" type="feather" iconStyle={styles.icon} />
        <View style={styles.textWrapper}>
          <Text style={styles.subTitle}>{bankAcc.name}</Text>
          <Text style={styles.content} selectable>
            {bankAcc.accNum}
          </Text>
        </View>
      </View>
    );
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.mainTitle}>Bank Accounts</Text>
      <View style={{...styles.card, ...tw`shadow-lg`}} selectable>
        {bankAccList.map((bankAcc, index) => {
          return <CardItemElement {...bankAcc} key={index} />;
        })}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  card: {
    borderRadius: 6,
    backgroundColor: '#fff',
    margin: 18,
    padding: 18,
    paddingBottom: 26,
  },
  cardItemWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginVertical: 12,
  },
  icon: {
    marginTop: 2,
    fontSize: 21
  },
  textWrapper: {
    marginHorizontal: 16,
  },
  mainTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    marginVertical: 24,
    textAlign: 'center',
  },
  subTitle: {fontSize: 16, fontWeight: 'bold', marginBottom: 4},
  content: {fontSize: 15},
});

export default BankAcc;
