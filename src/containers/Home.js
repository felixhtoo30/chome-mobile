import React, { useRef, useEffect } from 'react';
import { View, Text, BackHandler, Alert, ActivityIndicator } from 'react-native';
import { WebView, WebViewNavigation } from 'react-native-webview';

const mainURL = 'https://chome.centurylinksmm.com/';

const Home = ({ setInjectCode }) => {
  let webview = useRef(null);

  const handleNavigationState = (newNavState) => {
    const { url } = newNavState;
    if (!url) return;

    if (url.loading) {
      // const newURL = 'https://reactnative.dev/';
      // const redirectTo = 'window.location = "' + newURL + '"';
      // webview.injectJavaScript(redirectTo);
    }
  };

  const backAction = () => {
    if (webview.current.startUrl == mainURL) {
      confirmExitApp();
    } else {
      webview.current.goBack();
    }
    return true; // prevent default behavior (exiting app)
  };

  const confirmExitApp = () => {
    Alert.alert('Exit from CHome', 'Are u sure you want to exit?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {
        text: 'Yes',
        onPress: () => BackHandler.exitApp(),
      },
    ]);
    return true; // prevent default behavior (exiting app)
  };

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, []);

  if (webview.current) {
    setTimeout(() => {
      setInjectCode((injectCode) => {
        webview.current.injectJavaScript(injectCode);
        return ``;
      })
    }, 0);
  }


  return (
    <WebView
      ref={webview}
      source={{ uri: mainURL }}
      // onNavigationStateChange={handleNavigationState}
      onNavigationStateChange={(navState) => {
        console.log('loading >>', navState.loading);
        console.log('can go back >>', navState.canGoBack);
      }}
      startInLoadingState={true}
      renderLoading={() => <ActivityIndicator />}
      onMessage={() => { }}
    />
  );
};

export default Home;
